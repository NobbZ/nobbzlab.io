;;; blog --- renders my orgmode blog

;;; Commentary:

;;; Code:

(add-to-list 'load-path default-directory)

(setq base-directory default-directory)

(require 'org)
(require 'init-blog)

(org-publish-project "blog" t)

(provide 'blog)
;;; blog.el ends here
