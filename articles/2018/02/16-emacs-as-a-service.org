#+TITLE:  Emacs as a service
#+AUTHOR: Norbert Melzer
#+EMAIL:  timmelzer@gmail
#+DATE:   <2018-02-16 Fri>

After I have been asked to explain a bit about how I set up my emacs
in response to a [[https://elixirforum.com/t/which-tutorial-book-course-do-you-recommend-if-someone-is-learning-emacs-for-elixir-development/12511/14?u=nobbz][post I made in the elixirforum]] I do it now.

* Prerequisites

1. You are using ~systemd~.
2. You are using ~emacs~.

* Setting up a ~systemd~ service-unit

The following snippet has to be saved in ~${HOME}/.config/systemd/user/emacs.service~.

#+BEGIN_SRC systemd
  [Unit]
  Description=Emacs: the extensible, self-documenting text editor

  [Service]
  Type=forking
  ExecStart=/usr/bin/emacs --daemon
  ExecStop=/usr/bin/emacsclient --eval "(kill-emacs)"
  Environment=SSH_AUTH_SOCK=%t/keyring/ssh
  Restart=always

  [Install]
  WantedBy=default.target
#+END_SRC

After it is saved, you can enable the service via ~systemctl --user
enable emacs.service~, the emacs daemon process will from then on be
started when you log in to your system and be shut down when you log
of.

Next thing to do is to also start it for the current session, such
that you can use it right away. This is done via ~systemctl --user
start emacs.service~

If you change your emacs configuration you have to ~systemctl --user
restart emacs.service~.

If your deamon does not properly start, you probably need to add
~(server-start)~ to your ~.emacs~-file.

* Setting up your environment

Open up your shells RC file and add the equivalent of the following
snippet (which itself is from a ~.zshrc~ but should be valid bash
syntax as well).

#+BEGIN_SRC shell-script
  alias emax="emacsclient -c" # open file in a *new* X-Window
  alias emat="emacsclient -t" # open file in the current terminal
  export VISUAL="emacsclient -c"
  export EDITOR="emacsclient -t"
#+END_SRC

Thats it.

From now on do not use ~emacs~ or ~emacs -nw~ anymore, but the aliases
from above. Of course, if you had aliases before that you are more
familiar with, feel free to edit them accordingly to use ~emacsclient~
now.

* Afterword

** Without systemd?

Before I switched to [[https://www.archlinux.org/][arch-linux]] I used [[https://www.funtoo.org/Welcome][funtoo linux]] which did not use
systemd but openrc. Openrc did not have something equivalent to
systemds usermode. As a replacement I started emacs in deamon mode on
login from within my windowmanager.

How this has to be done depends on the windowmanager you use. Also
this of course does not work for SSH-logins.

And even though I do not have problems anymore yet, I'd like to learn
about solutions that work without systemd *and* without beeing able to
use ~sudo~ (so a system service does not count).

** From windowmanagers

I've heard that many windowmanagers have something similar to Windows
startmenu and manage it through desktop-files.

For those you'll need to find your emacs desktop file and change it to
use ~emacsclient -c~ to actually benefit of it from outside of your
terminal.

I do not use a windowmanager that has such a menu, but I start all my
applications through [[https://github.com/DaveDavenport/rofi][rofi]] and the emacsclient entries are rather
prominent in the list of recent applications.
