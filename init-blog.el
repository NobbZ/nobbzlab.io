;;; init-blog --- initialises stuff related to the blog generator

;;; Commentary:

;;; Code:

;; TODO: Move those into their own init-files as soon as I have discovered options to tweak
(require 'htmlize) ;; Needed to prettyprint code-blocks
(message "loaded htmlize v%s" htmlize-version)
(require 'yaml-mode)
(message "loaded yaml-mode v%s" (yaml-mode-version))
(require 'elixir-mode)
(message "loaded elixir-mode v%s" (elixir-mode-version))
(require 'systemd)

;; TODO: Move into its own package as well later on
(setq org-publish-project-alist
      `(("blog" ; The main project, kind of umbrella
         :components ("articles" "meta"))
        ("meta"
         :base-directory       ,(concat base-directory "blog/")
         :publishing-directory ,(concat base-directory "public/")
         :section-numbers      nil
         :table-of-contents    t
         :with-toc             1
         :recursive            t
         :publishing-function  org-html-publish-to-html)
        ("articles"                                                 ; The project name
         :base-directory       ,(concat base-directory "articles/") ; The base directory of the content
         :publishing-directory ,(concat base-directory "public/")   ; The directory to publish to
         :section-numbers      nil                                  ; Whether to show section numbers in headings
         :table-of-contents    t                                    ; Whether to show a table of content in each article
         :with-toc             1                                    ; The depth of the table of contents
         :recursive            t                                    ; scan folders in ~:base-directory~ recursively
         :publishing-function  org-html-publish-to-html             ; function to use when publishing
         :style "<link rel=\"stylesheet\"
                  href=\"../other/mystyle.css\"
                  type=\"text/css\"/>")))

(provide 'init-blog)
;;; init-blog.el ends here
