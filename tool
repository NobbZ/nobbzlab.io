#!/usr/bin/env elixir

case System.argv do
  [cmd|args] ->
    if File.regular?("bin/#{cmd}.exs") do
      System.argv(args)
      Code.eval_file("bin/#{cmd}.exs")
    else
      IO.puts "Command #{cmd} not available"
    end
  [] ->
    IO.puts "No arguments specified"
end
