EMACS_OPTS = --no-site-lisp --no-site-file --no-init-file --batch

ORG_FILES = $(wildcard blog/*.org)
EL_FILES = ${ORG_FILES:%.org=%.el}

CASK ?= cask

ORG_ARTICLES = $(shell find articles/ -type f \( -name '*.org' -not -name '.*' \))

.PHONY: clean org blog

blog: .cask-install index
	mkdir -p public
	cp -rT ./static ./public
	${CASK} exec emacs ${EMACS_OPTS} --load blog.el

clean: ${EL_FILES:%=%-clean}

index: articles/index.org

.cask-install: Cask
	${CASK} --verbose install
	touch $@

articles/index.org: $(filter-out articles/index.org, ${ORG_ARTICLES})
	./tool index

%-clean:
	rm -f $*
