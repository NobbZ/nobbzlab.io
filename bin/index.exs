defmodule Scanner do
  def find_articles do
    "articles"
    |> File.ls!
    |> Stream.map(&"articles/#{&1}")
    |> Stream.filter(&File.dir?/1)
    |> Stream.flat_map(&months/1)
    |> Stream.flat_map(&articles/1)
  end

  defp months(path) do
    path
    |> File.ls!
    |> Stream.map(&"#{path}/#{&1}")
    |> Stream.filter(&File.dir?/1)
  end

  defp articles(path) do
    path
    |> File.ls!
    |> Stream.map(&"#{path}/#{&1}")
    |> Stream.filter(&File.regular?/1)
    |> Stream.filter(&Regex.match?(~r'^articles/\d{4}/\d{2}/\d{2}-.+\.org$', &1))
  end
end

defmodule Org do
  defstruct [:path, :title, :author, :date]

  def eval(path) do
    "articles" <> link_path = path
    meta = read_meta(path)
    %Org{
      path: link_path,
      title: meta["TITLE"],
      author: meta["AUTHOR"],
      date: to_date(meta["DATE"])
    }
  end

  defp to_date(date) do
    [[_|date]] = Regex.scan(~r/<(\d{4})-(\d{2})-(\d{2}).*>/, date)
    date
    |> Enum.map(&String.to_integer/1)
    |> (&apply(Date, :new, &1)).()
    |> elem(1)
  end

  defp read_meta(path) do
    path
    |> File.read!()
    |> String.split("\n")
    |> Enum.reduce_while(%{}, fn
      "#+" <> line, acc ->
	[[_, key, val]] = Regex.scan(~r/(.*):(.*)/, line)
        {:cont, Map.put(acc, key, String.trim(val))}
      _, acc ->
	{:halt, acc}
    end)
  end
end

months = %{
  1 => "January",
  2 => "February",
  3 => "March",
  4 => "April",
  5 => "May",
  6 => "June",
  7 => "July",
  8 => "August",
  9 => "Septembre",
  10 => "Octobre",
  11 => "Novembre",
  12 => "Decembre"
}

Scanner.find_articles
|> Enum.map(&Org.eval/1)
|> Enum.group_by(fn e -> e.date.year end)
|> Enum.map(fn {y, l} ->
  {y, Enum.group_by(l, fn e -> e.date.month end)}
end)
|> (&EEx.eval_string("""
#+TITLE:  NobbZ' Blog Index
#+AUTHOR: Norbert Melzer
#+EMAIL:  timmelzer@gmail.com

<%= for {y, l} <- @docs do %>
* <%= y %>
<%= for {m, l} <- Enum.sort_by(l, &elem(&1, 0)) do %>
** <%= @months[m] %>
<%= for a <- l do %>
- [[https://nobbz.gitlab.io<%= Regex.replace(~r/\.org/, a.path, ".html") %>][<%= a.title %>]] (<%= a.author %>) 
<% end %><% end %><% end %>
""", assigns: [docs: &1, months: months])).()
|> (&File.write!("articles/index.org", &1)).()
